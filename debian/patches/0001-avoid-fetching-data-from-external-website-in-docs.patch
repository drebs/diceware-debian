From: Andre Bianchi <drebs@riseup.net>
Date: Sun, 22 Oct 2017 16:35:50 -0200
Subject: avoid fetching data from external website in docs

Lintian considers it a "privacy breach" if a package fetches data from
external websites at runtime. In this case, the documentation contains
badges (for ci build, etc) and a link to an xkcd image that illustrates
the purpose of diceware. I am opting for removing those instead of
creating lintian overrides because:

- including static badges don't make much sense as it's meant to be an
  up-to-date visual feedback of the state of dev.

- i couldn't include the xkcd image in the package as it's distributed
  under CC-BY-NC 2.5 which is not dfsg compliant.

- a link for the xkcd image is already included in the documentation
  text.
---
 README.rst     | 28 ++--------------------------
 docs/index.rst |  7 -------
 2 files changed, 2 insertions(+), 33 deletions(-)

diff --git a/README.rst b/README.rst
index 5b5d634..5e01142 100644
--- a/README.rst
+++ b/README.rst
@@ -3,27 +3,7 @@ diceware
 
 Passphrases to remember...
 
-|bdg-tests|  | `documentation <https://diceware.readthedocs.io/>`_ | `sources <https://github.com/ulif/diceware>`_ | `issues <https://github.com/ulif/diceware/issues>`_
-
-.. |bdg-tests| image:: https://github.com/ulif/diceware/actions/workflows/tests.yml/badge.svg?branch=master
-   :target: https://github.com/ulif/diceware/actions/workflows/tests.yml
-   :alt: Test Status
-
-.. |bdg-last-release| image:: https://img.shields.io/pypi/v/diceware.svg
-    :target: https://pypi.python.org/pypi/diceware/
-    :alt: Latest Release
-
-.. |bdg-versions| image:: https://img.shields.io/pypi/pyversions/diceware.svg
-    :target: https://pypi.python.org/pypi/diceware/
-    :alt: Supported Python Versions
-
-.. |bdg-license| image:: https://img.shields.io/pypi/l/diceware.svg
-    :target: https://pypi.python.org/pypi/diceware/
-    :alt: License
-
-.. |doc-status| image:: https://readthedocs.io/projects/diceware/badge/?version=latest
-     :target: https://diceware.readthedocs.io/en/latest/
-     :alt: Documentation Status
+`documentation <https://diceware.readthedocs.io/>`_ | `sources <https://github.com/ulif/diceware>`_ | `issues <https://github.com/ulif/diceware/issues>`_
 
 `diceware` is a passphrase generator following the proposals of
 Arnold G. Reinhold on http://diceware.com . It generates passphrases
@@ -239,11 +219,7 @@ What is it good for?
 Normally, `diceware` passphrases are easier to remember than shorter
 passwords constructed in more or less bizarre ways. But at the same
 time `diceware` passphrases provide more entropy as `xkcd`_ can show
-with the famous '936' proof_:
-
-.. image:: http://imgs.xkcd.com/comics/password_strength.png
-   :align: center
-   :target: http://xkcd.com/936/
+with the famous '936' proof_.
 
 .. _xkcd: http://xkcd.com/
 .. _proof: http://xkcd.com/936/
diff --git a/docs/index.rst b/docs/index.rst
index af9a648..74c7b1d 100644
--- a/docs/index.rst
+++ b/docs/index.rst
@@ -8,9 +8,6 @@ Welcome to `diceware` documentation
 
 Version: |version|
 
-|bdg-tests|
-
-
 .. toctree::
    :maxdepth: 2
 
@@ -28,7 +25,3 @@ Indices and tables
 * :ref:`genindex`
 * :ref:`modindex`
 * :ref:`search`
-
-.. |bdg-tests| image:: https://github.com/ulif/diceware/actions/workflows/tests.yml/badge.svg?branch=master
-   :target: https://github.com/ulif/diceware/actions/workflows/tests.yml
-   :alt: Test Status
